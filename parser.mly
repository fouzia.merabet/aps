%{
open Ast
%}
%token <int> NUM
%token <string> IDENT
%token PLUS MINUS TIMES DIV EQUAL LESS
%token LPAR RPAR
%token EOL
%token LCRO RCRO
%token OR AND NEG TRUE FALSE
%token BOOLEAN INTEGER VOID
%token ECHO
%token CONSTANT FUNCTION RECUR
%token SC VER SEP AFF
%token DPOINT
%token IF
%token VARIABLE AFFECT PROCEDURE COND LOOP APPEL
%token LTH NTH ALLOC VEC
%token RETURN
%start line             /* the entry point */

%type <Ast.prog > prog
%type <Ast.prog> line


%%

	line:
	prog EOL                     { $1 }
	;
	
	
prog:
	LCRO cmds RCRO  { ASTProg($2) }	
	;
	
cmds:
	stat {ASTCmds($1)}
	| dec SC cmds {ASTCmdsDec($1,$3)}
	| stat SC cmds {ASTCmdsStat($1,$3)}
	| ret {ASTCmdsRet($1)}
	;
	
lval :
    LPAR NTH lval expr RPAR 	{ASTLval($3,$4)}
	|IDENT						{ASTId($1)}
	
	 ;	
ret :
  RETURN expr          {ASTRet($2)}
typee : 
     BOOLEAN  		     {ASTTprim(Ast.Bool) }
	 | VOID							{ASTTprim(Ast.Void)}
   | INTEGER		     {ASTTprim(Ast.Int)}
   | LPAR VEC typee RPAR {ASTVec($3)}
   |LPAR types AFF typee  RPAR {ASTType($2,$4)}       
   ;	

types:
	typee      {[$1]}
	| typee SEP types	{($1::$3) }
	;


arg: 
   IDENT DPOINT typee {ASTArg($1,$3) }
   ;


args:
	arg  		{[$1]}
	| arg VER args  {($1::$3) }
	;

block:
	LCRO cmds RCRO  { ASTBlock($2) }	
	;
	  
dec:
	|VARIABLE IDENT typee				{ASTVari($2,$3)}
	|CONSTANT IDENT typee expr			{ASTConst($2,$3,$4)} 
	|FUNCTION IDENT typee LCRO args RCRO expr	{ASTFun($2,$3,$5,$7)}
	|FUNCTION RECUR IDENT typee LCRO args RCRO expr	{ASTFunRec($3,$4,$6,$8)}
	|PROCEDURE IDENT LCRO args RCRO block   {ASTProc($2,$4,$6)}
	|PROCEDURE RECUR  IDENT LCRO args RCRO block   {ASTProcRec($3,$5,$7)}
	|FUNCTION IDENT typee LCRO args RCRO block 	{ASTFunP($2,$3,$5,$7)}
	|FUNCTION RECUR IDENT typee LCRO args RCRO  block {ASTFunRecP($3,$4,$6,$8)}

;

stat:
	ECHO expr		{ASTEcho($2)}
	|AFFECT lval expr {ASTSet($2,$3)}
	| COND expr block block {ASTCond($2,$3,$4)}
	| LOOP expr block {ASTLoop($2,$3)}
	| APPEL IDENT exprs {ASTAppel ($2,$3)}

	;


expr:
	  TRUE						 { ASTTrue }	
	| FALSE						 { ASTFalse }						
	| NUM                        { ASTNum($1) }
	| IDENT                      { ASTId($1) }
	| LPAR PLUS expr expr RPAR   { ASTPrim(Ast.Add, $3, $4) }
	| LPAR MINUS expr expr RPAR  { ASTPrim(Ast.Sub, $3, $4) }
	| LPAR TIMES expr expr RPAR  { ASTPrim(Ast.Mul, $3, $4) }
	| LPAR DIV expr expr RPAR    { ASTPrim(Ast.Div, $3, $4) }
	| LPAR EQUAL expr expr RPAR  { ASTPrim(Ast.Eq, $3, $4) }
	| LPAR LESS expr expr RPAR   { ASTPrim(Ast.Lt, $3, $4) }
	| LPAR AND expr expr RPAR    { ASTPrim(Ast.And, $3, $4) }
	| LPAR OR expr expr RPAR     { ASTPrim(Ast.Or, $3, $4) }
	| LPAR NEG expr RPAR         { ASTUPrim(Ast.Not, $3) }
	| LCRO args RCRO expr	     { ASTArgs($2, $4)}
	| LPAR IF expr expr expr RPAR	 { ASTIf($3,$4,$5)}
	| LPAR expr exprs RPAR		 { ASTExprs($2,$3)}
	| LPAR LTH expr RPAR		 { ASTLth($3)}
	| LPAR ALLOC expr RPAR		 { ASTAlloc($3)}
	| LPAR NTH expr expr RPAR	 { ASTNth($3, $4)}

	;
 
exprs:
	expr						 {[$1]}
	|expr exprs					 {($1::$2)}
