open Ast
open Printf


let gen_add = let c = ref 0 in (fun () -> incr c; "a"^(string_of_int !c));;

type value = InN of int 
    | ()
		|InA of string 
		|InB of string * int
		|InF of expr * (string list)* env   (* env c'est pour capter l environement à l instant ou la fonction a été declaréé *)
		|InFR of expr * (string list)* env * string
		|InP of block * (string list)* env 
		|InPR of block * (string list)* env * string 


and value2 =InN of int
			|InA of string
			
and value3 =InN of int

and env  = (string * value) list

and mem  = (string * value2 ref ) list

and flot = (value3) list
;;


let rec listarg e = 
	match e with 
		|[] -> []
		|ASTArg(x,t)::f -> x::(listarg f)
		;; 
			


let pi op e =
	match op with 
		| "not"  -> begin match e with
						| (0,_)  -> 1
						| (1,_)  -> 0 end
		| "and" -> begin match e with 
							| (0 , n) -> 0
							| (n , 0) -> 0 
							| (n , 1) -> n
							| (1 , n) -> n
							end	
		| "or" -> begin match e with 
							| (0 , n) -> n
							| (n , 0) -> n 
							| (n , 1) -> 1
							| (1 , n) -> 1
							end
		| "eq" -> begin match e with 
							| (n1 , n2) when n1=n2 -> 1
							| (n1 , n2) when n1<>n2 -> 0
							end
		| "lt" ->begin match e with 
							| (n1 , n2) when n1<n2 -> 1
							| (n1 , n2) when n1>= n2 -> 0
							end
		| "add" ->begin match e with (n1 , n2) -> n1 + n2  end 
		| "sub" ->begin match e with (n1 , n2) -> n1 - n2 end 
		| "mul" ->begin match e with (n1 , n2) -> n1 * n2 end 
		| "div" ->begin match e with (n1 , n2) -> n1 / n2 end



 (* fonction manipulant l environement *)

and v n =
	n


and appendEnv env e =
	List.append [e] env

and appendMem mem e =
	List.append [e] mem

and appendFlot flot e =
	List.append [e] flot
;;
let rec printEnv env =
	match env with
	[] -> print_string "\n";
	| (k,v)::q ->( print_string k;
				print_string "\n";
				printEnv q
				)
;;
let allocn mem n =
	let adr = gen_add() in
	let mem1 = ref [] in
	mem1 := appendMem mem (adr,ref(InN(-1)));
	
	for i = 2 to n do
		let ad = gen_add() in
		mem1 := appendMem !mem1 (ad,ref(InN(-1))) 
	done;
	(adr,!mem1)
;;

let rec appendArgsValsEnv env1 mem flot (args,vals) =
	match (args,vals) with 
	[],[] -> []
	|x::xs, y::ys -> let (v,mem,flot) =(eval_expr env1 mem flot y) in
						 (x, v)::(appendArgsValsEnv env1 mem flot (xs,ys)) 


and getValEnv env x =
	match env with
	[] -> raise Not_found
	| (k,v)::_ when k = x -> v
	| (k,v)::q when k != x ->
	 getValEnv q x

and getValMem mem x =
	match mem with
	[] -> raise Not_found
	| (k,v)::_ when k = x -> !v
	| _::q -> getValMem q x

and setValMem mem x v1=
	match mem with
	[] ->	 raise Not_found
	| (k,v)::x_ when k = x  -> v := v1 
	| (k1,v)::q -> setValMem q x v1


and eval_expr env mem flot e =
	match e with 
			ASTTrue -> (InN(1),mem,flot)
			|ASTFalse -> (InN(0),mem,flot)
			|ASTNum n -> (InN(v(n)),mem,flot)
			|ASTId x -> ( let v1 = getValEnv env x in
							match v1 with 
							InN(x) -> (InN(x),mem,flot)
							|InA(a)-> ((getValMem mem a),mem,flot) 
							|InB(a,n)-> ((InB(a,n)),mem,flot)
							|InF(e, args, env) -> (InF(e, args, env),mem,flot)
							|InFR(e, args, env,e2)-> (InFR(e, args, env,e2),mem,flot)
							|InP(b, args, env) ->  (InP(b, args, env),mem,flot)
							|InPR(b, args, env,e) -> (InPR(b, args, env,e),mem,flot)
			)
			|ASTPrim(op, e1, e2) -> let x = (string_of_op op)
									and ((InN(v1)),mem,flot1) = eval_expr env mem flot e1
									and ((InN(v2)),mem2,flot2) = eval_expr env mem flot e2
									in (InN(pi x (v1, v2)),mem2,flot2)
			|ASTUPrim(uop, e1) ->  let x =  (string_of_uop uop)
									and  (InN(v),mem,flot1) = eval_expr env mem flot e1
									in (InN (pi x (v,1)),mem,flot) 	
									
			|ASTIf(e1,e2,e3) ->( if (eval_expr env mem flot e1) = (InN(1),mem,flot) then 
								let (InN(v1),mem,flot1) = (eval_expr env mem flot e2)
								in (InN(v1),mem,flot1)
								else 
								let (InN(v2),mem,flot1) = (eval_expr env mem flot e3)
								in (InN(v2),mem,flot1)
			)
			|ASTArgs(ee1,ee2) -> (
					let args = listarg ee1 in
					(InF(ee2,args,env),mem,flot)
			)
			|ASTExprs(e1,e2) ->(
				match e1 with 
					|ASTExprs(ee1,ee2) ->(
						let ASTId(ee3) = ee1 in
						let ee = getValEnv env ee3 in
						match ee with 
						|InF(e,args,envC) -> (let new_env =  appendArgsValsEnv env mem flot (args,ee2) in
						let new_env1 = List.append envC new_env in
						eval_expr new_env1 mem  flot (ASTExprs(e,e2))
						)
				
					)
					|ASTArgs(ee1,ee2) -> (
						let args = listarg ee1 in
						let new_env =  appendArgsValsEnv env mem flot (args,e2) in
						let new_env1 = List.append env new_env in
						let (InN(x),mem,flot1) =(eval_expr new_env1 mem  flot ee2) in
						(InN(x),mem,flot1)
					)
					| ASTId (f) ->(
						let e = getValEnv env f in
						match e with
						| InF(e,args,envC) -> ( let new_env =  appendArgsValsEnv env mem flot (args,e2) in
												let new_env1 = List.append envC new_env in
												(eval_expr new_env1 mem flot e)
						)
						|InFR(e,args,envC,x) -> ( let new_env =  appendArgsValsEnv env mem flot (args,e2) in
												let new_env2 = appendEnv new_env  (x,InFR(e,args,envC,x)) in
												let new_env3 = List.append envC new_env2   in
												(eval_expr new_env3 mem flot e)					
						) 
						| InP(e,args,envC) -> ( let new_env =  appendArgsValsEnv env mem flot (args,e2) in
						let new_env1 = List.append envC new_env in
						(eval_block new_env1 mem flot e)
							)
							|InPR(e,args,envC,x) -> ( let new_env =  appendArgsValsEnv env mem flot (args,e2) in
							let new_env2 = appendEnv new_env  (x,InPR(e,args,envC,x)) in
							let new_env3 = List.append envC new_env2   in
							(eval_block new_env3 mem flot e)					
	) 
					)
					
			)
			|ASTAlloc(e1) -> (
				let (InN(n),mem,flot1) = eval_expr env mem  flot e1 in
				let (adr,mem) = allocn mem n in
				(InB(adr,n),mem,flot1)
				
			)
			|ASTNth(e1,e2) -> (
				let (InB(a,n),mem,flot1) = eval_expr env mem flot e1 in
				let (InN(i),mem,flot2) = eval_expr env mem flot1 e2 in
				let nadr = String.sub a 1 ((String.length a)-1) in
				let add = "a"^(string_of_int((int_of_string nadr)+ i)) in
				(getValMem mem add,mem,flot2)

			)
			|ASTLth(e1) ->(
				let (InB(a,n),mem,flot1) = eval_expr env mem flot e1 in
				(InN(n),mem,flot1)

			)

	


and eval_exprlist env mem flot e =
		match e with
			[] -> []
			|d::f ->(	 
				(eval_expr env mem flot d)::(eval_exprlist env mem flot f) 
			)


and eval_stat env mem flot e =
	match e with 
			ASTEcho(e1) -> let (InN(n),mem,flot1) = (eval_expr env mem flot e1) in (

					 Printf.printf "%d\n" n;
					 let x =appendFlot flot1 n in 
					 ((),mem,x)
			)

			|ASTSet(e1,e2) -> (let (adr,mem,flot1) =(eval_lval env mem flot e1) in
								let (ev,mem,flot2) =(eval_expr env mem flot1 e2 )in
								setValMem mem adr ev;
								((),mem,flot2)
								
			)
			|ASTCond(e1,e2,e3) -> (if (eval_expr env mem flot e1) = (InN(1),mem,flot) then (
								let (ev,mem,flot1)=eval_block env mem flot e2 in
								(ev,mem,flot1)

			)
								else (
									let (ev,mem,flot1)=eval_block env mem flot e3 in
									(ev,mem,flot1)
								)
			)
			|ASTLoop(e1,e2) -> ( while (eval_expr env mem flot e1) = (InN(1),mem,flot) do
								eval_block env mem flot e2;
								done;
								((),mem,flot)
			)
			|ASTAppel(e1,e2) -> (
	
				let ee = getValEnv env e1 in
				match ee with
				| InP(bk,args,envC) -> (
					 let new_env =  appendArgsValsEnv env mem flot (args,e2) in
					 let new_env1 = List.append envC new_env in
					 eval_block new_env1 mem flot bk 
				)
				|InPR(bk,args,envC,x) -> (	
					 
					 let new_env =  appendArgsValsEnv env mem flot (args,e2) in
					 let new_env2 =	appendEnv new_env (x,InPR(bk,args,envC,x))in
					 let new_env3 = List.append envC new_env2 in
					eval_block new_env3 mem flot bk 
				) 
			)
and eval_lval env mem flot e =
	match e with 
	ASTLval(e1,e2) ->(
		let (a,mem,flot) = eval_lval env mem flot e1 in
		let (InN(i),mem,flot) = eval_expr env mem flot e2 in
		let nadr = String.sub a 1 ((String.length a)-1) in
		let add = "a"^(string_of_int((int_of_string nadr)+ i)) in
		(add,mem,flot)

	)
	|ASTId(e1) ->( let ads = (getValEnv env e1) in
			match ads with 
			InA(a) -> (a,mem,flot)
			|InB(a,n) -> (a,mem,flot)
	)

and eval_block env mem flot e =
	match e with 
	|ASTBlock(e1) -> (
		eval_cmds env mem  flot e1 
	)

and eval_dec env mem  flot e =
	match e with 
		|ASTConst(e1,e2,e3)-> let (v,mem,flot1) = (eval_expr env mem flot e3) in  
							(let newEnv = appendEnv env (e1,v) in
							 (newEnv,mem,flot1))
		|ASTFun(e1,e2,e3,e4) -> let args = listarg e3 in
								let newEnv = appendEnv env (e1,InF(e4,args,env))in
								(newEnv,mem,flot)
		|ASTFunRec(e1,e2,e3,e4) -> let args = listarg e3 in
							let newEnv = appendEnv env (e1,InFR(e4,args,env,e1))in
							(newEnv,mem,flot)
  	
		
		
		|ASTVari(e1,e2) -> (let adr = gen_add() in 
						let newMem = appendMem mem (adr,ref(InN(-1))) in
						let newEnv = appendEnv env (e1,(InA(adr))) in
						(newEnv,newMem,flot)
						)
		|ASTProc(e1,e2,e3) -> let args = listarg e2 in
								let newEnv = appendEnv env (e1,InP(e3,args,env))in
								(newEnv,mem,flot)
		|ASTProcRec(e1,e2,e3) -> let args = listarg e2 in

							let newEnv =  appendEnv env (e1,InPR(e3,args,env,e1))  in
							(newEnv,mem,flot)
							
		|ASTFunP(e1,e2,e3,e4) -> let args = listarg e3 in
							let newEnv = appendEnv env (e1,InP(e4,args,env))in
							(newEnv,mem,flot)
		|ASTFunRecP(e1,e2,e3,e4) -> let args = listarg e3 in
							let newEnv = appendEnv env (e1,InPR(e4,args,env,e1))in
							(newEnv,mem,flot)


and eval_ret env mem flot e=
	match e with
	  |ASTRet(e1)->(
			let (envP,memP,flot1)=eval_expr env mem flot e1 
			in eval_expr env mem flot1 e1 ;
			(envP,mem,flot1)
		)
and eval_cmds env mem flot e =
	
	match e with 
		
	  ASTCmds(e1)->(
			eval_stat env mem flot e1
		)
		|ASTCmdsDec(e1,e2)->(
			let (envP,memP,flot) = eval_dec env mem flot e1 in
			eval_cmds envP memP flot e2

		)
		|ASTCmdsStat(e1,e2)->(
			let (v,mem,flot) = eval_stat env mem  flot e1 in
			eval_cmds env mem flot e2
					)
		|ASTCmdsRet(e1)->(
			eval_ret env mem flot e1;
			
		)


and eval_prog env mem flot e =
match e with
	|ASTProg(e1)->(
		eval_cmds env mem flot e1
)

let _ = 
	try 
		let f = open_in "test.aps" in 
		let lexbuf = Lexing.from_channel f in 
		let e = Parser.line Lexer.token lexbuf in
		eval_prog [] [] [] e;
		print_char '\n'
	with Lexer.Eof -> exit 0
	
;;
