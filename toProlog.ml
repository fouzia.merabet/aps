open Ast
open Printf

let rec print_prolog e =
	match e with 
			ASTTrue -> Printf.printf "true"
			|ASTFalse-> Printf.printf "false"
			|ASTNum n ->Printf.printf "num";
				Printf.printf "("; 
				Printf.printf"%d" n;
				Printf.printf ")";
			|ASTId x -> Printf.printf"ident(%s)" x
			|ASTPrim(op, e1, e2) -> (
				Printf.printf"%s" (string_of_op op);
				Printf.printf "(";
				print_prolog e1;
				Printf.printf ",";
				print_prolog e2;
				Printf.printf ")"
			)
			|ASTUPrim(uop, e1) -> (
				Printf.printf"%s" (string_of_uop uop);
				Printf.printf "(";
				print_prolog e1;
				Printf.printf ")"
			
			)
			|ASTIf(e1,e2,e3) -> (
				Printf.printf "if";
				Printf.printf "(";
				print_prolog e1;
				Printf.printf ",";
				print_prolog e2;
				Printf.printf ",";
				print_prolog e3;
				Printf.printf ")";	
			 
			)
			| ASTArgs(e1,e2) ->(
				Printf.printf "argexpr";
				Printf.printf "(";
				Printf.printf "[";
				print_prologarglist e1;
				Printf.printf "]";
				Printf.printf ",";
				print_prolog e2;
				Printf.printf ")";
					
			)
			|ASTExprs(e1,e2) ->(
				Printf.printf "exprs";
				Printf.printf "(";
				Printf.printf "[";
				print_prolog e1;
				Printf.printf ",";
				print_prologexprlist e2;
				Printf.printf "]";
				Printf.printf ")";
				
			)
			|ASTLth(e1) ->(
			Printf.printf "len";
			Printf.printf "(";
			print_prolog e1;
			Printf.printf ")";			
			)
			|ASTAlloc(e1) ->(
			Printf.printf "alloc";
			Printf.printf "(";
			print_prolog e1;
			Printf.printf ")";			
			)
			|ASTNth(e1,e2) ->(
			Printf.printf "nth";
			Printf.printf "(";
			print_prolog e1;
			Printf.printf ",";
			print_prolog e2;
			Printf.printf ")";			
			)
			

			
and print_prologexprlist e =
		match e with
			[] -> ()
			|d::[] -> print_prolog d
			|d::f ->(	   			
				print_prolog d; 
				Printf.printf ",";
				print_prologexprlist f 
			)

and print_prologarg e =	
		match e with 
			ASTArg(e1,e2) ->(
				(*Printf.printf "arg";*)
				Printf.printf "(";
				Printf.printf "ident(%s)" e1;
				Printf.printf ",";
				print_prologtype e2;
				Printf.printf ")";
			)

and print_prologarglist e =
       match e with 
				[] -> ()
				| d::[] -> print_prologarg d
				| d::f ->( 
					print_prologarg d;
					Printf.printf ",";
					print_prologarglist f;
				)


and print_prologLval e =
	 match e with 
	 
	 ASTLval(e1,e2) -> (
		 				Printf.printf "lval";
						Printf.printf "(";
						print_prologLval e1;
						Printf.printf ",";
						print_prolog e2;
						Printf.printf ")";
	 )
		|ASTId(e1) -> (
		 				Printf.printf "ident(%s)" e1;
	 )


and print_prologstat e =	
		match e with 
			ASTEcho(e1) -> (
				Printf.printf "echo";
				Printf.printf "(";
				print_prolog e1;
				Printf.printf ")";			
			)
			|ASTSet(e1,e2)->(
			Printf.printf "set";
			Printf.printf "(";
			print_prologLval e1;
			Printf.printf ",";
			print_prolog e2;
			Printf.printf ")"
			)
			|ASTCond(e1,e2,e3)->(
			Printf.printf "cond";
			Printf.printf "(";
			print_prolog e1;
			Printf.printf ",";
			print_prologblock e2;
			Printf.printf ",";
			print_prologblock e3;
			Printf.printf ")"
			)
			|ASTLoop(e1,e2)->(
			Printf.printf "whilee";
			Printf.printf "(";
			print_prolog e1;
			Printf.printf ",";
			print_prologblock e2;
			Printf.printf ")"
			)
			|ASTAppel(e1,e2)->(
			Printf.printf "call";
			Printf.printf "(";
			Printf.printf "ident(%s)" e1;
			Printf.printf ",";
			Printf.printf "[";
			print_prologexprlist e2;
			Printf.printf "]";
			Printf.printf ")"
			)
			


and print_prologtype e =
	match e with 
	       ASTType(e1,e2) ->(
	            Printf.printf "fleche";
	            Printf.printf "(";
				Printf.printf "[";
				print_listtype e1;
				Printf.printf "]";
				Printf.printf ",";
				print_prologtype e2;
				Printf.printf ")";
				)
	       | ASTTprim(e1)-> (
	            Printf.printf"%s" (string_of_tprim e1) ;
	       )
				|ASTVec(e1) ->(

					Printf.printf "vec";
	        Printf.printf "(";
					print_prologtype e1;
					Printf.printf ")";
				
				 )

			
and print_listtype e = 
	match e with 
		|[] -> ()
		|d::[] -> print_prologtype d
		|d::f -> 
			print_prologtype d;
			Printf.printf ",";
			print_listtype f;
	
and print_prologdec e =
	match e with 
		|ASTVari(e1,e2) -> (
			Printf.printf "var";
			Printf.printf "(";
			Printf.printf "ident(%s)" e1;
			Printf.printf ",";
			print_prologtype e2;
			Printf.printf ")"

		)

		|ASTConst(e1,e2,e3)->(
			Printf.printf "const";
			Printf.printf "(";
			Printf.printf "ident(%s)" e1;
			Printf.printf ",";
			print_prologtype e2;
			Printf.printf ",";
			print_prolog e3;
			Printf.printf ")"

		)
		|ASTFun(e1,e2,e3,e4) ->(
			Printf.printf "fun";
			Printf.printf "(";
			Printf.printf "ident(%s)" e1;
			Printf.printf ",";
			print_prologtype e2;
			Printf.printf ",";
			Printf.printf "[";
			print_prologarglist e3;
			Printf.printf "]";
			Printf.printf ",";
			print_prolog e4;
			Printf.printf ")"
		)
		|ASTFunRec(e1,e2,e3,e4) ->(
			Printf.printf "funRec";
			Printf.printf "(";
			Printf.printf "ident(%s)" e1;
			Printf.printf ",";
			print_prologtype e2;
			Printf.printf ",";
			print_prologarglist e3;
			Printf.printf ",";
			print_prolog e4;
			Printf.printf ")"
		)
		| ASTProc (e1,e2,e3) ->(
			Printf.printf "proc";
			Printf.printf "(";
			Printf.printf "ident(%s)" e1;
			Printf.printf ",";
			Printf.printf "[";
			print_prologarglist e2;
			Printf.printf "]";
			Printf.printf ",";
			print_prologblock  e3;
			Printf.printf ")"
		)
		| ASTProcRec (e1,e2,e3) ->(
			Printf.printf "procrec";
			Printf.printf "(";
			Printf.printf "ident(%s)" e1;
			Printf.printf ",";
			Printf.printf "[";
			print_prologarglist e2;
			Printf.printf "]";
			Printf.printf ",";
			print_prologblock  e3;
			Printf.printf ")"
		
		)
		|ASTFunP(e1,e2,e3,e4) ->(
			Printf.printf "funp";
			Printf.printf "(";
			Printf.printf "ident(%s)" e1;
			Printf.printf ",";
			print_prologtype e2;
			Printf.printf ",";
			Printf.printf "[";
			print_prologarglist e3;
			Printf.printf "]";
			Printf.printf ",";
			print_prologblock e4;
			Printf.printf ")"
		)
		|ASTFunRecP(e1,e2,e3,e4) ->(
			Printf.printf "funRecp";
			Printf.printf "(";
			Printf.printf "ident(%s)" e1;
			Printf.printf ",";
			print_prologtype e2;
			Printf.printf ",";
			print_prologarglist e3;
			Printf.printf ",";
			print_prologblock e4;
			Printf.printf ")"
		)

and print_prologblock e=
    match e with
	  |ASTBlock(e1)->(
		Printf.printf "block";
		Printf.printf  "(";
		print_prologcmds e1;
		Printf.printf ")"

	)
and print_prologret e =
		 match e with 
		 |ASTRet(e1)->(
		 Printf.printf "ret";
		 Printf.printf  "(";			
			print_prolog e1;
			Printf.printf ")"
		 )
and print_prologcmds e =
	match e with 
		|ASTCmds(e1)->(
			Printf.printf "stat";
			Printf.printf  "(";			
			print_prologstat e1;
			Printf.printf ")"

		)
		|ASTCmdsDec(e1,e2)->(
			Printf.printf "decCmds";
			Printf.printf  "(";
			print_prologdec e1;
			Printf.printf ",";
			print_prologcmds e2;
			Printf.printf ")"

		)
		|ASTCmdsStat(e1,e2)->(
			Printf.printf "statCmds";
			Printf.printf  "(";
			print_prologstat e1;
			Printf.printf  ",";
			print_prologcmds e2;
			Printf.printf  ")"
		)
		|ASTCmdsRet (e1)->(
			print_prologret e1;
		)

and print_prologprog e =
match e with
	|ASTProg(e1)->(
		Printf.printf "prog";
		Printf.printf  "(";
		print_prologcmds e1;
		Printf.printf ")"

)


		
			
let _ = 
	try 
		let f = open_in "test.aps" in 
		let lexbuf = Lexing.from_channel f in 
		let e = Parser.line Lexer.token lexbuf in
		print_prologprog e;
		print_char '\n'
	with Lexer.Eof -> exit 0
