type op = Add | Mul | Sub | Div | And | Or | Lt | Eq
type uop = Not 
type tprim = Bool | Int | Vec | Void

let string_of_op op =
 match op with
	  Add -> "add"
	| Mul -> "mul"
	| Sub -> "sub"
	| Div -> "div"
	| And -> "and"
	| Or  -> "or"
	| Lt  -> "lt"
	| Eq  -> "eq"


let op_of_string op =
 match op with
	  "add" -> Add
	| "mul" -> Mul
	| "sub" -> Sub
	| "div" -> Div
	| "and" -> And
	|  "or" -> Or 
	| "lt"  -> Lt
	| "eq"  -> Eq


let string_of_tprim p =
 match p with
    Bool ->"bool"
    |Int ->"int"
		|Vec -> "vec"
 		|Void -> "void"

let tprim_of_string p =
 match p with
      "bool" -> Bool
    |"int" -> Int
		|"vec" -> Vec
		|"void" -> Void
 			
 		
let string_of_uop uop =
 match uop with 
		Not -> "not"
		
		
let uop_of_string uop =
 match uop with
	  "not" -> Not
	
	
	
	
type prog =
	ASTProg of cmds 

and cmds =
	ASTCmds of stat
	|ASTCmdsDec of dec * cmds
	|ASTCmdsStat of stat * cmds		
	|ASTCmdsRet of ret

and block = 
  	ASTBlock of cmds 



and dec = 
	ASTVari of string * typee
	|ASTConst of string * typee * expr
	|ASTFun of string * typee * args * expr
	|ASTFunRec of string * typee * args * expr
	|ASTFunP of string * typee * args * block
	|ASTFunRecP of string * typee * args * block
	|ASTProc of string * args * block
	|ASTProcRec of string * args * block

and lval = 
	ASTId of string
	|ASTLval of lval * expr

and ret = ASTRet of expr

and types = typee list
	
and typee= 
	ASTTprim of tprim
    |ASTType of types * typee
    |ASTVec of typee 	
     
and arg =
	ASTArg of string * typee

and args = arg list

and stat =
	ASTEcho of expr
	|ASTSet of lval * expr
	|ASTCond of expr *block* block
	|ASTLoop of expr * block
	|ASTAppel of string * exprs



and expr =
		ASTTrue
	|	ASTFalse
	| ASTNum of int
	| ASTId of string
	| ASTPrim of op * expr * expr
	| ASTUPrim of uop * expr
	| ASTBool of bool
	| ASTArgs of args * expr 
	| ASTIf of expr * expr * expr
	| ASTExprs of expr * exprs
	| ASTLth of expr 
	| ASTAlloc of expr
	| ASTNth of expr * expr
and exprs = 
		expr list

;;
