/*------------------------------
 Fonctions utiles pour les lists
 -------------------------------*/
appends([],TS,TS).
appends([(Ar,T)|L],LS,TS):- append([T],LS,TS), appends(L,TS,Y).

extract(X,[X|_]).
extract(X,[_|XS]) :- extract(X,XS).

/*verifie si E est de type T*/
check(C,[],[]).
check(C,[E|ES],[T|TS]):- typeExpr(C,E,T), check(C,ES,TS).


/*------------ 
Type de base 
-------------*/
typeExpr(C,T,T).
typeExpr(C,num(X),int).
typeExpr(C,ident(X),T) :- extract((ident(X),T),C).
/*------------
 Expressions 
 ------------*/

typeExpr(C,not(X), bool)  :- typeExpr(C,X,bool).
typeExpr(C,and(X,Y),bool) :- typeExpr(C,X,bool), typeExpr(C,Y,bool).
typeExpr(C,or(X,Y),bool)  :- typeExpr(C,X,bool), typeExpr(C,Y,bool).
typeExpr(C,eq(X,Y),bool)  :- typeExpr(C,X,int) ,  typeExpr(C,Y,int).
typeExpr(C,lt(X,Y),bool)  :- typeExpr(C,X,int) , typeExpr(C,Y,int).

typeExpr(C,add(X,Y),int) :- typeExpr(C,X,int), typeExpr(C,Y,int).
typeExpr(C,sub(X,Y),int) :- typeExpr(C,X,int), typeExpr(C,Y,int).
typeExpr(C,mul(X,Y),int) :- typeExpr(C,X,int), typeExpr(C,Y,int).
typeExpr(C,div(X,Y),int) :- typeExpr(C,X,int), typeExpr(C,Y,int).


typeExpr(C,if(A,B,D),T) :- typeExpr(C,A,bool), typeExpr(C,B,T), typeExpr(C,D,T).

typeExpr(C,argexpr(Args,E),flesh(TS,T)) :- appends(Args,[],TS), append(C,Args,Cc),typeExpr(Cc,E,T).

typeExpr(C,exprs([E|ES]),T) :- typeExpr(C,E,flesh(TS,T)),  check(C,ES,TS).




/*---------
 Statement 
 ----------*/
typeStat(C,echo(X),T):- typeExpr(C,X,int).

typeStat(C,set(X,E),void):- typeExpr(C,X,T),  typeExpr(C,E,T).
/*IF0*/
typeStat(C,cond(E1,E2,E3),T):- typeExpr(C,E1,bool),  typeBlock(C,E2,T), typeBlock(C,E3,T).
/*IF1*/
typeStat(C,cond(E1,E2,E3),(void,T)):- not(T=void), typeExpr(C,E1,bool),  typeBlock(C,E2,void), typeBlock(C,E3,T).
/*IF2*/
typeStat(C,cond(E1,E2,E3),(void,T)):- not(T=void), typeExpr(C,E1,bool),  typeBlock(C,E2,T), typeBlock(C,E3,void).

typeStat(C,whilee(E,B),(void,T)):- typeExpr(C,E,bool), typeBlock(C,B,T).

typeStat(C,call(X,A),void):- typeExpr(C,X,flesh(TS,void)),  check(C,A,TS).

/*-----------
 Declaration 
 ------------*/

typeDec(C,const(ident(I),T,E),[(ident(I),T)|C]) :- typeExpr(C,E,T).

typeDec(C,fun(ident(X),T,A,E),[(ident(X),flesh(TS,T))|C]) :- appends(A,[],TS), append(C,A,Cc) , typeExpr(Cc,E,T).

typeDec(C,funRec(ident(X),T,A,E),[(ident(X),flesh(TS,T))|C]):- appends(A,[],TS), append(C,A,Cc) , append(Cc,(ident(X),flesh(TS,T)),Cx), typeExpr(Cx,E,T).

typeDec(C,var(ident(I),T),[(ident(I),T)|C]).

typeDec(C,proc(ident(X),A,B),[(ident(X),flesh(TS,void))|C]) :- appends(A,[],TS), append(C,A,Cc) , typeBlock(Cc,B,void).

typeDec(C,procrec(ident(X),A,B),[(ident(X),flesh(TS,void))|C]) :- appends(A,[],TS), append(C,A,Cc) , append(Cc,(ident(X),flesh(TS,T)),Cx), typeExpr(Cx,B,void).

typeDec(C,funp(ident(X),T,A,B),[(ident(X),flesh(TS,T))|C]) :- appends(A,[],TS), append(C,A,Cc) , typeBlock(Cc,B,T).

typeDec(C,funRecp(ident(X),T,A,B),[(ident(X),flesh(TS,T))|C]) :- appends(A,[],TS), append(C,A,Cc) , append(Cc,(ident(X),flesh(TS,T)),Cx), typeBlock(Cx,B,T).

/*---------
 Block 
 ----------*/
typeBlock(C,block(X),T):- typeCmds(C,X,T).

/*------------------
Suites de commandes
-------------------*/
typeCmds(C,ret(X),T) :- typeExpr(C,X,T).

typeCmds(C,stat(X),T) :- typeStat(C,X,T).

typeCmds(C,decCmds(D,S),T) :- typeDec(C,D,Cc),typeCmds(Cc,S,T).
/*STAT0*/
typeCmds(C,statCmds(Sc,S),T) :- typeStat(C,Sc,void), typeCmds(C,S,T).
/*STAT1*/
typeCmds(C,statCmds(Sc,S),T) :- not(T = void), typeStat(C,Sc,T), typeCmds(C,S,T).


/*----------
Program
------------*/
typeProg(C,prog(P),T) :- typeCmds(C,P,T).
