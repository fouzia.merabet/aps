type token =
  | NUM of (int)
  | IDENT of (string)
  | PLUS
  | MINUS
  | TIMES
  | DIV
  | EQUAL
  | LESS
  | LPAR
  | RPAR
  | EOL
  | LCRO
  | RCRO
  | OR
  | AND
  | NEG
  | TRUE
  | FALSE
  | BOOLEAN
  | INTEGER
  | VOID
  | ECHO
  | CONSTANT
  | FUNCTION
  | RECUR
  | SC
  | VER
  | SEP
  | AFF
  | DPOINT
  | IF
  | VARIABLE
  | AFFECT
  | PROCEDURE
  | COND
  | LOOP
  | APPEL
  | LTH
  | NTH
  | ALLOC
  | VEC
  | RETURN

val line :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Ast.prog
