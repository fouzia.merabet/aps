type token =
  | NUM of (int)
  | IDENT of (string)
  | PLUS
  | MINUS
  | TIMES
  | DIV
  | EQUAL
  | LESS
  | LPAR
  | RPAR
  | EOL
  | LCRO
  | RCRO
  | OR
  | AND
  | NEG
  | TRUE
  | FALSE
  | BOOLEAN
  | INTEGER
  | VOID
  | ECHO
  | CONSTANT
  | FUNCTION
  | RECUR
  | SC
  | VER
  | SEP
  | AFF
  | DPOINT
  | IF
  | VARIABLE
  | AFFECT
  | PROCEDURE
  | COND
  | LOOP
  | APPEL
  | LTH
  | NTH
  | ALLOC
  | VEC
  | RETURN

open Parsing;;
let _ = parse_error;;
# 2 "parser.mly"
open Ast
# 50 "parser.ml"
let yytransl_const = [|
  259 (* PLUS *);
  260 (* MINUS *);
  261 (* TIMES *);
  262 (* DIV *);
  263 (* EQUAL *);
  264 (* LESS *);
  265 (* LPAR *);
  266 (* RPAR *);
  267 (* EOL *);
  268 (* LCRO *);
  269 (* RCRO *);
  270 (* OR *);
  271 (* AND *);
  272 (* NEG *);
  273 (* TRUE *);
  274 (* FALSE *);
  275 (* BOOLEAN *);
  276 (* INTEGER *);
  277 (* VOID *);
  278 (* ECHO *);
  279 (* CONSTANT *);
  280 (* FUNCTION *);
  281 (* RECUR *);
  282 (* SC *);
  283 (* VER *);
  284 (* SEP *);
  285 (* AFF *);
  286 (* DPOINT *);
  287 (* IF *);
  288 (* VARIABLE *);
  289 (* AFFECT *);
  290 (* PROCEDURE *);
  291 (* COND *);
  292 (* LOOP *);
  293 (* APPEL *);
  294 (* LTH *);
  295 (* NTH *);
  296 (* ALLOC *);
  297 (* VEC *);
  298 (* RETURN *);
    0|]

let yytransl_block = [|
  257 (* NUM *);
  258 (* IDENT *);
    0|]

let yylhs = "\255\255\
\001\000\002\000\003\000\003\000\003\000\003\000\007\000\007\000\
\006\000\009\000\009\000\009\000\009\000\009\000\010\000\010\000\
\011\000\012\000\012\000\013\000\005\000\005\000\005\000\005\000\
\005\000\005\000\005\000\005\000\004\000\004\000\004\000\004\000\
\004\000\008\000\008\000\008\000\008\000\008\000\008\000\008\000\
\008\000\008\000\008\000\008\000\008\000\008\000\008\000\008\000\
\008\000\008\000\008\000\008\000\014\000\014\000\000\000"

let yylen = "\002\000\
\002\000\003\000\001\000\003\000\003\000\001\000\005\000\001\000\
\002\000\001\000\001\000\001\000\004\000\005\000\001\000\003\000\
\003\000\001\000\003\000\003\000\003\000\004\000\007\000\008\000\
\006\000\007\000\007\000\008\000\002\000\003\000\004\000\003\000\
\003\000\001\000\001\000\001\000\001\000\005\000\005\000\005\000\
\005\000\005\000\005\000\005\000\005\000\004\000\004\000\006\000\
\004\000\004\000\004\000\005\000\001\000\002\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\000\000\055\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\006\000\001\000\036\000\037\000\000\000\000\000\
\034\000\035\000\029\000\000\000\000\000\000\000\000\000\008\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\009\000\
\002\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\010\000\012\000\011\000\
\000\000\000\000\000\000\021\000\000\000\030\000\000\000\000\000\
\000\000\000\000\032\000\000\000\033\000\005\000\004\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\022\000\000\000\000\000\000\000\000\000\
\000\000\000\000\031\000\054\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\046\000\000\000\050\000\000\000\
\051\000\049\000\017\000\019\000\047\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\020\000\038\000\039\000\
\040\000\041\000\042\000\043\000\045\000\044\000\000\000\052\000\
\013\000\016\000\000\000\000\000\000\000\007\000\025\000\000\000\
\048\000\014\000\000\000\023\000\027\000\000\000\026\000\024\000\
\028\000"

let yydgoto = "\002\000\
\004\000\005\000\106\000\017\000\018\000\019\000\034\000\076\000\
\098\000\099\000\059\000\060\000\074\000\077\000"

let yysindex = "\010\000\
\254\254\000\000\132\255\000\000\009\255\087\255\020\255\002\255\
\023\255\014\255\006\255\087\255\087\255\027\255\087\255\017\255\
\008\255\030\255\000\000\000\000\000\000\000\000\122\255\056\255\
\000\000\000\000\000\000\051\255\051\255\060\255\051\255\000\000\
\043\255\087\255\057\255\084\255\086\255\086\255\087\255\000\000\
\000\000\132\255\132\255\087\255\087\255\087\255\087\255\087\255\
\087\255\087\255\087\255\087\255\087\255\087\255\087\255\087\255\
\087\255\071\255\075\255\093\255\250\254\000\000\000\000\000\000\
\087\255\098\255\051\255\000\000\014\255\000\000\056\255\101\255\
\132\255\086\255\000\000\087\255\000\000\000\000\000\000\087\255\
\087\255\087\255\087\255\087\255\087\255\087\255\087\255\104\255\
\087\255\106\255\087\255\107\255\109\255\051\255\056\255\087\255\
\051\255\092\255\103\255\000\000\056\255\110\255\087\255\120\255\
\056\255\128\255\000\000\000\000\125\255\133\255\134\255\135\255\
\136\255\137\255\138\255\139\255\000\000\087\255\000\000\140\255\
\000\000\000\000\000\000\000\000\000\000\141\255\051\255\051\255\
\144\255\056\255\148\255\086\255\146\255\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\153\255\000\000\
\000\000\000\000\160\255\091\255\158\255\000\000\000\000\086\255\
\000\000\000\000\031\255\000\000\000\000\091\255\000\000\000\000\
\000\000"

let yyrindex = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\159\255\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\162\255\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\081\255\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\147\255\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000"

let yygindex = "\000\000\
\000\000\000\000\009\000\000\000\000\000\000\000\104\000\250\255\
\246\255\050\000\000\000\187\255\219\255\204\255"

let yytablesize = 177
let yytable = "\027\000\
\075\000\104\000\061\000\029\000\093\000\037\000\038\000\035\000\
\040\000\003\000\001\000\016\000\062\000\063\000\064\000\032\000\
\057\000\065\000\066\000\020\000\068\000\028\000\033\000\108\000\
\031\000\124\000\030\000\070\000\039\000\041\000\036\000\129\000\
\058\000\042\000\097\000\133\000\107\000\080\000\081\000\082\000\
\083\000\084\000\085\000\086\000\087\000\088\000\089\000\090\000\
\091\000\092\000\078\000\079\000\006\000\007\000\008\000\043\000\
\102\000\058\000\100\000\061\000\149\000\067\000\009\000\010\000\
\011\000\012\000\013\000\014\000\071\000\062\000\063\000\064\000\
\015\000\109\000\110\000\111\000\112\000\113\000\114\000\115\000\
\116\000\069\000\118\000\123\000\120\000\072\000\126\000\021\000\
\022\000\125\000\053\000\021\000\022\000\053\000\151\000\023\000\
\131\000\073\000\024\000\023\000\094\000\095\000\155\000\025\000\
\026\000\096\000\053\000\025\000\026\000\101\000\157\000\143\000\
\105\000\117\000\159\000\119\000\121\000\147\000\122\000\127\000\
\161\000\130\000\021\000\022\000\044\000\045\000\046\000\047\000\
\048\000\049\000\023\000\128\000\132\000\024\000\135\000\050\000\
\051\000\052\000\025\000\026\000\134\000\156\000\136\000\137\000\
\138\000\139\000\140\000\141\000\142\000\144\000\145\000\160\000\
\053\000\006\000\007\000\008\000\148\000\150\000\152\000\054\000\
\055\000\056\000\153\000\009\000\010\000\011\000\012\000\013\000\
\014\000\154\000\158\000\003\000\103\000\015\000\018\000\015\000\
\146\000"

let yycheck = "\006\000\
\038\000\071\000\009\001\002\001\057\000\012\000\013\000\002\001\
\015\000\012\001\001\000\003\000\019\001\020\001\021\001\002\001\
\023\000\028\000\029\000\011\001\031\000\002\001\009\001\076\000\
\002\001\095\000\025\001\034\000\002\001\013\001\025\001\101\000\
\002\001\026\001\041\001\105\000\074\000\044\000\045\000\046\000\
\047\000\048\000\049\000\050\000\051\000\052\000\053\000\054\000\
\055\000\056\000\042\000\043\000\022\001\023\001\024\001\026\001\
\067\000\002\001\065\000\009\001\130\000\002\001\032\001\033\001\
\034\001\035\001\036\001\037\001\012\001\019\001\020\001\021\001\
\042\001\080\000\081\000\082\000\083\000\084\000\085\000\086\000\
\087\000\039\001\089\000\094\000\091\000\002\001\097\000\001\001\
\002\001\096\000\010\001\001\001\002\001\013\001\132\000\009\001\
\103\000\012\001\012\001\009\001\030\001\027\001\012\001\017\001\
\018\001\013\001\026\001\017\001\018\001\012\001\148\000\118\000\
\012\001\010\001\152\000\010\001\010\001\128\000\010\001\028\001\
\158\000\012\001\001\001\002\001\003\001\004\001\005\001\006\001\
\007\001\008\001\009\001\029\001\013\001\012\001\010\001\014\001\
\015\001\016\001\017\001\018\001\013\001\148\000\010\001\010\001\
\010\001\010\001\010\001\010\001\010\001\010\001\010\001\158\000\
\031\001\022\001\023\001\024\001\013\001\010\001\013\001\038\001\
\039\001\040\001\010\001\032\001\033\001\034\001\035\001\036\001\
\037\001\010\001\013\001\013\001\069\000\042\001\013\001\029\001\
\127\000"

let yynames_const = "\
  PLUS\000\
  MINUS\000\
  TIMES\000\
  DIV\000\
  EQUAL\000\
  LESS\000\
  LPAR\000\
  RPAR\000\
  EOL\000\
  LCRO\000\
  RCRO\000\
  OR\000\
  AND\000\
  NEG\000\
  TRUE\000\
  FALSE\000\
  BOOLEAN\000\
  INTEGER\000\
  VOID\000\
  ECHO\000\
  CONSTANT\000\
  FUNCTION\000\
  RECUR\000\
  SC\000\
  VER\000\
  SEP\000\
  AFF\000\
  DPOINT\000\
  IF\000\
  VARIABLE\000\
  AFFECT\000\
  PROCEDURE\000\
  COND\000\
  LOOP\000\
  APPEL\000\
  LTH\000\
  NTH\000\
  ALLOC\000\
  VEC\000\
  RETURN\000\
  "

let yynames_block = "\
  NUM\000\
  IDENT\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : Ast.prog ) in
    Obj.repr(
# 29 "parser.mly"
                              ( _1 )
# 300 "parser.ml"
               : Ast.prog))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'cmds) in
    Obj.repr(
# 34 "parser.mly"
                 ( ASTProg(_2) )
# 307 "parser.ml"
               : Ast.prog ))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'stat) in
    Obj.repr(
# 38 "parser.mly"
      (ASTCmds(_1))
# 314 "parser.ml"
               : 'cmds))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'dec) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'cmds) in
    Obj.repr(
# 39 "parser.mly"
               (ASTCmdsDec(_1,_3))
# 322 "parser.ml"
               : 'cmds))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'stat) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'cmds) in
    Obj.repr(
# 40 "parser.mly"
                (ASTCmdsStat(_1,_3))
# 330 "parser.ml"
               : 'cmds))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'ret) in
    Obj.repr(
# 41 "parser.mly"
       (ASTCmdsRet(_1))
# 337 "parser.ml"
               : 'cmds))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'lval) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 45 "parser.mly"
                             (ASTLval(_3,_4))
# 345 "parser.ml"
               : 'lval))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 46 "parser.mly"
             (ASTId(_1))
# 352 "parser.ml"
               : 'lval))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 50 "parser.mly"
                       (ASTRet(_2))
# 359 "parser.ml"
               : 'ret))
; (fun __caml_parser_env ->
    Obj.repr(
# 52 "parser.mly"
                     (ASTTprim(Ast.Bool) )
# 365 "parser.ml"
               : 'typee))
; (fun __caml_parser_env ->
    Obj.repr(
# 53 "parser.mly"
               (ASTTprim(Ast.Void))
# 371 "parser.ml"
               : 'typee))
; (fun __caml_parser_env ->
    Obj.repr(
# 54 "parser.mly"
                   (ASTTprim(Ast.Int))
# 377 "parser.ml"
               : 'typee))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'typee) in
    Obj.repr(
# 55 "parser.mly"
                         (ASTVec(_3))
# 384 "parser.ml"
               : 'typee))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 3 : 'types) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'typee) in
    Obj.repr(
# 56 "parser.mly"
                               (ASTType(_2,_4))
# 392 "parser.ml"
               : 'typee))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'typee) in
    Obj.repr(
# 60 "parser.mly"
            ([_1])
# 399 "parser.ml"
               : 'types))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'typee) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'types) in
    Obj.repr(
# 61 "parser.mly"
                   ((_1::_3) )
# 407 "parser.ml"
               : 'types))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'typee) in
    Obj.repr(
# 66 "parser.mly"
                      (ASTArg(_1,_3) )
# 415 "parser.ml"
               : 'arg))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'arg) in
    Obj.repr(
# 71 "parser.mly"
        ([_1])
# 422 "parser.ml"
               : 'args))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'arg) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'args) in
    Obj.repr(
# 72 "parser.mly"
                 ((_1::_3) )
# 430 "parser.ml"
               : 'args))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'cmds) in
    Obj.repr(
# 76 "parser.mly"
                 ( ASTBlock(_2) )
# 437 "parser.ml"
               : 'block))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'typee) in
    Obj.repr(
# 80 "parser.mly"
                          (ASTVari(_2,_3))
# 445 "parser.ml"
               : 'dec))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 2 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'typee) in
    let _4 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 81 "parser.mly"
                              (ASTConst(_2,_3,_4))
# 454 "parser.ml"
               : 'dec))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 5 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 4 : 'typee) in
    let _5 = (Parsing.peek_val __caml_parser_env 2 : 'args) in
    let _7 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 82 "parser.mly"
                                           (ASTFun(_2,_3,_5,_7))
# 464 "parser.ml"
               : 'dec))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 5 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 4 : 'typee) in
    let _6 = (Parsing.peek_val __caml_parser_env 2 : 'args) in
    let _8 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 83 "parser.mly"
                                                 (ASTFunRec(_3,_4,_6,_8))
# 474 "parser.ml"
               : 'dec))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 4 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 2 : 'args) in
    let _6 = (Parsing.peek_val __caml_parser_env 0 : 'block) in
    Obj.repr(
# 84 "parser.mly"
                                         (ASTProc(_2,_4,_6))
# 483 "parser.ml"
               : 'dec))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 4 : string) in
    let _5 = (Parsing.peek_val __caml_parser_env 2 : 'args) in
    let _7 = (Parsing.peek_val __caml_parser_env 0 : 'block) in
    Obj.repr(
# 85 "parser.mly"
                                                (ASTProcRec(_3,_5,_7))
# 492 "parser.ml"
               : 'dec))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 5 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 4 : 'typee) in
    let _5 = (Parsing.peek_val __caml_parser_env 2 : 'args) in
    let _7 = (Parsing.peek_val __caml_parser_env 0 : 'block) in
    Obj.repr(
# 86 "parser.mly"
                                             (ASTFunP(_2,_3,_5,_7))
# 502 "parser.ml"
               : 'dec))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 5 : string) in
    let _4 = (Parsing.peek_val __caml_parser_env 4 : 'typee) in
    let _6 = (Parsing.peek_val __caml_parser_env 2 : 'args) in
    let _8 = (Parsing.peek_val __caml_parser_env 0 : 'block) in
    Obj.repr(
# 87 "parser.mly"
                                                   (ASTFunRecP(_3,_4,_6,_8))
# 512 "parser.ml"
               : 'dec))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 92 "parser.mly"
            (ASTEcho(_2))
# 519 "parser.ml"
               : 'stat))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'lval) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 93 "parser.mly"
                   (ASTSet(_2,_3))
# 527 "parser.ml"
               : 'stat))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'block) in
    let _4 = (Parsing.peek_val __caml_parser_env 0 : 'block) in
    Obj.repr(
# 94 "parser.mly"
                         (ASTCond(_2,_3,_4))
# 536 "parser.ml"
               : 'stat))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'block) in
    Obj.repr(
# 95 "parser.mly"
                   (ASTLoop(_2,_3))
# 544 "parser.ml"
               : 'stat))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : string) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'exprs) in
    Obj.repr(
# 96 "parser.mly"
                     (ASTAppel (_2,_3))
# 552 "parser.ml"
               : 'stat))
; (fun __caml_parser_env ->
    Obj.repr(
# 102 "parser.mly"
              ( ASTTrue )
# 558 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    Obj.repr(
# 103 "parser.mly"
               ( ASTFalse )
# 564 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : int) in
    Obj.repr(
# 104 "parser.mly"
                              ( ASTNum(_1) )
# 571 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 105 "parser.mly"
                              ( ASTId(_1) )
# 578 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 106 "parser.mly"
                              ( ASTPrim(Ast.Add, _3, _4) )
# 586 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 107 "parser.mly"
                              ( ASTPrim(Ast.Sub, _3, _4) )
# 594 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 108 "parser.mly"
                              ( ASTPrim(Ast.Mul, _3, _4) )
# 602 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 109 "parser.mly"
                              ( ASTPrim(Ast.Div, _3, _4) )
# 610 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 110 "parser.mly"
                              ( ASTPrim(Ast.Eq, _3, _4) )
# 618 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 111 "parser.mly"
                              ( ASTPrim(Ast.Lt, _3, _4) )
# 626 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 112 "parser.mly"
                              ( ASTPrim(Ast.And, _3, _4) )
# 634 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 113 "parser.mly"
                              ( ASTPrim(Ast.Or, _3, _4) )
# 642 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 114 "parser.mly"
                              ( ASTUPrim(Ast.Not, _3) )
# 649 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 2 : 'args) in
    let _4 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 115 "parser.mly"
                            ( ASTArgs(_2, _4))
# 657 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 3 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _5 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 116 "parser.mly"
                                ( ASTIf(_3,_4,_5))
# 666 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'exprs) in
    Obj.repr(
# 117 "parser.mly"
                          ( ASTExprs(_2,_3))
# 674 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 118 "parser.mly"
                        ( ASTLth(_3))
# 681 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 119 "parser.mly"
                          ( ASTAlloc(_3))
# 688 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _3 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _4 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 120 "parser.mly"
                            ( ASTNth(_3, _4))
# 696 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 125 "parser.mly"
            ([_1])
# 703 "parser.ml"
               : 'exprs))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'exprs) in
    Obj.repr(
# 126 "parser.mly"
                  ((_1::_2))
# 711 "parser.ml"
               : 'exprs))
(* Entry line *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let line (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : Ast.prog)
