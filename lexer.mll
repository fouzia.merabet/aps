
{
open Parser        (* The type token is defined in parser.mli *)
exception Eof
}

rule token = parse
  [' ' '\t']       { token lexbuf }     (* skip blanks *)
| ['\n' ]          { EOL }
| ['0'-'9']+('.'['0'-'9'])? as lxm { NUM(int_of_string lxm) }
| "add"            { PLUS }
| "sub"            { MINUS }
| "mul"            { TIMES }
| "div"            { DIV }
| '('              { LPAR }
| ')'              { RPAR }
| eof              { raise Eof }
| '['			   { LCRO}
| ']'			   { RCRO}
| ';'				{ SC }
| ','				{ VER }
| '*'				{ SEP }
| "->"				{ AFF }
| "eq"				{ EQUAL }
| "lt"				{ LESS }
| "not"				{ NEG }
| "and"				{ AND }
| "or"				{ OR }
| "true"			{ TRUE }
| "false"			{ FALSE }
| "int"				{ INTEGER }
| "bool"			{ BOOLEAN }
| "void"      { VOID }
| "ECHO"			{ ECHO }
| "REC"				{ RECUR }
| "FUN"				{ FUNCTION }
| "CONST"			{ CONSTANT }
| "VAR"       {VARIABLE}
| "SET"       {AFFECT}
| "PROC"      {PROCEDURE}
| "IF"        {COND}
| "WHILE"     {LOOP}
| "CALL"      {APPEL}
| "len"       { LTH }
| "nth"       { NTH }
| "alloc"     { ALLOC }
| "vec"       { VEC }
| "RETURN"    { RETURN }
| ':'				{ DPOINT}
| "if"				{ IF	}
|['a'-'z' 'A'-'Z']['a'-'z' 'A'-'Z' '0'-'9']*  as lxm{ IDENT(lxm)}
